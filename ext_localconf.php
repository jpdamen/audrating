<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

# --- BEGIN: Shop --- #

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'AUDIBENE.' . $_EXTKEY,
    'Rating',
    array(
        'Rating' => 'index',
    ),
    // non-cacheable actions
    array(
  		'Rating' => 'index',
    ),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

# --- END: Shop --- #