<?php

namespace AUDIBENE\Audrating\Controller;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class AbstractController extends ActionController {

	protected $contentObject;

	public function initializeObject() {

        # --- get objectmanager and cObj --- #
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManager');
        $this->contentObject = $this->configurationManager->getContentObject();
   
   }

}