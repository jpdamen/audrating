<?php
namespace AUDIBENE\Audrating\Controller;

class RatingController extends AbstractController {

	/**
	* @var \AUDIBENE\Audrating\Service\RatingService
	* @inject
	*/
	protected $ratingService;

	/**
	 * @return void
	 */
	public function indexAction() {

		list($average,$count,$percentage) = $this->ratingService->getRating();

		$this->view->assignMultiple(
			array(
				'average' => $average,
				'count' => $count,
				'percentage' => $percentage
			));

	}
}