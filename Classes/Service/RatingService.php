<?php
namespace AUDIBENE\Audrating\Service;

class RatingService extends AbstractService {

	const JSURL = 'https://d3q9bnsmwljuux.cloudfront.net/badges/de/audibene-gmbh.js';

	protected $finder;

	public function getRating() {

		$this->stripJavascript();
		$average = $this->getAverage();
		$count = $this->getCount();
		$percentage = ((double)$average/5)*100;
		return array($average,$count,$percentage);
	}

	protected function stripJavascript() {

		$content = file_get_contents(self::JSURL);
		$content = str_replace('document.write("', '', $content);
		$content = str_replace(');', '', $content);
		$content = str_replace('\n', '', $content);
		$content = str_replace('\\', '', $content);
		$doc = new \DOMDocument();
		$doc->loadHTML($content);
		$this->finder = new \DomXPath($doc);

	}

	protected function getAverage() {
		$classname="salesworker-rate";
		$average = $this->getNode($classname);
		preg_match_all('/[0-5]{1}.[0-9]{1}/', $average, $matches);
		return $matches[0][0];
	}

	protected function getCount() {
		$classname="salesworker-count";
		return $this->getNode($classname);

	}

	protected function getNode($classname) {
		$nodes = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
		return $nodes->item(0)->nodeValue;
	}


}