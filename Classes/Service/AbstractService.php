<?php
namespace AUDIBENE\Audrating\Service;
use TYPO3\CMS\Core\SingletonInterface;

class AbstractService implements SingletonInterface {

	protected $objectManager;
	
	public function __construct() {

		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
        
   }

}