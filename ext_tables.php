<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

# --- FlexForms Default --- #

$defaultShowItemPrefix = '
        --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.general;general,header,
    --div--;Configuration,
        ';

$defaultShowItemPostfix = ',
    --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.appearance,
        --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.frames;frames,
        --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.textlayout;textlayout,
    --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
        --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility,tx_gridelements_container,tx_gridelements_columns,
        --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,
    --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended';

# --- FlexForms Default  --- #

# --- Rating --- #

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginSignature = $extensionName . '_rating';
$pluginIcon = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/rating.gif';
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Rating',
	'+ Rating',
	$pluginIcon
);
\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon('tt_content', $pluginSignature, $pluginIcon);

$showItem = $defaultShowItemPrefix;

$showItem .= $defaultShowItemPostfix;

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature]['showitem'] = $showItem;

unset($showItem);

# --- Rating --- #

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Audibene Rating');
